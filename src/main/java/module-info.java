module net.wizarddigital.tikiticker {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.apache.logging.log4j;
    requires opml.parser;
    requires com.apptasticsoftware.rssreader;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires commons.validator;

    opens net.wizarddigital.tikiticker to javafx.fxml;
    exports net.wizarddigital.tikiticker;
    exports net.wizarddigital.tikiticker.gui;
    opens net.wizarddigital.tikiticker.controller to javafx.fxml;
    opens net.wizarddigital.tikiticker.gui to javafx.fxml;
    exports net.wizarddigital.tikiticker.model to com.fasterxml.jackson.databind;
}