/*
TikiTicker
Copyright (C) 2023 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.wizarddigital.tikiticker.gui;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.wizarddigital.tikiticker.model.Settings;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class NewsTicker extends VBox {
    private final Logger LOGGER = LogManager.getLogger(this.getClass());
    HBox hbox;
    TranslateTransition transition;
    ArrayList<Hyperlink> hyperlinks = new ArrayList<>();
    ArrayList<Label> separatorLabels = new ArrayList<>();
    int scrollSpeed;
    int horizontalSpacing;
    Font font;
    String fontColorInHex;
    String backgroundColorInHex;
    boolean useItemSeparatorString;
    String itemSeparatorString;
    boolean useTransparentBackground;
    private Stage stage;
    Timeline animationDurationMinder;

    public NewsTicker(Stage stage) {
        this.stage = stage;
        this.setAlignment(Pos.CENTER);
        hbox = new HBox();
        hbox.setSpacing(5);
        hbox.setAlignment(Pos.CENTER_LEFT);
        hbox.setMaxWidth(Region.USE_PREF_SIZE);
        hbox.setMinWidth(Region.USE_PREF_SIZE);
        this.getChildren().add(hbox);
        buildAnimation();
        stage.widthProperty().addListener((obs, oldVal, newVal) -> {
            buildAnimation();
            restartAnimation();
        });
    }

    public int getScrollSpeed() {
        return scrollSpeed;
    }

    public void setScrollSpeed(int scrollSpeed) {
        this.scrollSpeed = scrollSpeed;
    }

    public int getHorizontalSpacing() {
        return horizontalSpacing;
    }

    public void setHorizontalSpacing(int horizontalSpacing) {
        this.horizontalSpacing = horizontalSpacing;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public String getFontColorInHex() {
        return fontColorInHex;
    }

    public void setFontColorInHex(String fontColorInHex) {
        this.fontColorInHex = fontColorInHex;
    }

    public String getBackgroundColorInHex() {
        return backgroundColorInHex;
    }

    public void setBackgroundColorInHex(String backgroundColorInHex) {
        this.backgroundColorInHex = backgroundColorInHex;
    }

    public boolean isUseItemSeparatorString() {
        return useItemSeparatorString;
    }

    public void setUseItemSeparatorString(boolean useItemSeparatorString) {
        this.useItemSeparatorString = useItemSeparatorString;
    }

    public String getItemSeparatorString() {
        return itemSeparatorString;
    }

    public void setItemSeparatorString(String itemSeparatorString) {
        this.itemSeparatorString = itemSeparatorString;
    }

    public boolean isUseTransparentBackground() {
        return useTransparentBackground;
    }

    public void setUseTransparentBackground(boolean useTransparentBackground) {
        this.useTransparentBackground = useTransparentBackground;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void loadSettings(Settings settings, boolean rebuildAnimation) {
        this.setUseItemSeparatorString(settings.getUseItemSeparatorString());
        this.setItemSeparatorString(settings.getItemSeparatorString());
        this.setFont(new Font(settings.getFontName(), settings.getFontSize()));
        this.setHorizontalSpacing(settings.getHorizontalSpacing());
        this.setScrollSpeed(settings.getScrollSpeed());
        this.setBackgroundColorInHex(settings.getBackgroundColorInHex());
        this.setFontColorInHex(settings.getFontColorInHex());
        this.setUseTransparentBackground(settings.isUseTransparentBackground());
        applySettings(rebuildAnimation);
    }

    public void applySettings(boolean rebuildAnimation) {
        LOGGER.log(Level.DEBUG, "Applying settings to NewsTicker");
        this.applyFont(font);
        hbox.setSpacing(horizontalSpacing);
        updateSeparatorLabels(itemSeparatorString);
        if (!useTransparentBackground) {
            applyBackgroundColor(backgroundColorInHex);
        }
        applyFontColor(fontColorInHex);
        if (rebuildAnimation) {
            buildAnimation();
            restartAnimation();
        }
    }

    public void addContextMenu(ContextMenu contextMenu) {
        this.setOnContextMenuRequested(event -> {
            contextMenu.show(this, event.getScreenX(), event.getScreenY());
        });
    }

    public void addHyperlink(Hyperlink hyperlink) {
        hyperlink.setFont(font);
        hyperlink.setStyle("-fx-text-fill: " + fontColorInHex + ";");
        this.hyperlinks.add(hyperlink);
        this.hbox.getChildren().add(hyperlink);
    }

    public void addSeparatorLabel(Label separatorLabel) {
        separatorLabel.setFont(font);
        separatorLabel.setStyle("-fx-text-fill: " + fontColorInHex + ";");
        this.separatorLabels.add(separatorLabel);
        this.hbox.getChildren().add(separatorLabel);
    }

    public void addHyperlinks(ArrayList<Hyperlink> links) {
        String itemSeparatorLabelText = "";
        if (useItemSeparatorString) {
            itemSeparatorLabelText = itemSeparatorString;
        }
        if (links.size() == 0) {
            for (int i = 0; i < 4; i++) {
                addHyperlink(new Hyperlink("No Feeds to Load"));
                addSeparatorLabel(new Label(itemSeparatorLabelText));
            }
            return;
        }
        for (Hyperlink link : links) {
            addHyperlink(link);
            addSeparatorLabel(new Label(itemSeparatorLabelText));
        }
    }

    public void applyFont(Font font) {
        for (Hyperlink hyperlink : hyperlinks) {
            hyperlink.setFont(font);
        }
        for (Label label : separatorLabels) {
            label.setFont(font);
        }
    }

    public void applyFontColor(String colorInHex) {
        for (Hyperlink hyperlink : hyperlinks) {
            hyperlink.setStyle("-fx-text-fill: " + colorInHex + ";");
        }
        for (Label label : separatorLabels) {
            label.setStyle("-fx-text-fill: " + colorInHex + ";");
        }
    }

    public void applyBackgroundColor(String colorInHex) {
        this.setStyle("-fx-background-color: " + colorInHex + ";");
    }

    public void activateInvisibleBackground() {
        this.setStyle("fx-background-color: transparent");
        hbox.setStyle("fx-background-color: transparent");
    }

    public void clearAllLinksAndLabels() {
        for (Hyperlink link : hyperlinks) {
            hbox.getChildren().remove(link);
        }
        for (Label separator : separatorLabels) {
            hbox.getChildren().remove(separator);
        }
    }

    public void updateSeparatorLabels(String separatorString) {
        for (Label label : separatorLabels) {
            label.setText(separatorString);
        }
    }

    public void buildAnimation() {
        LOGGER.log(Level.DEBUG, "Building NewsTicker animation");
        transition = new TranslateTransition();
        transition.setInterpolator(Interpolator.LINEAR);
        transition.setCycleCount(Timeline.INDEFINITE);
        transition.setNode(hbox);
        transition.setFromX(stage.getWidth());
        LOGGER.log(Level.DEBUG, "Animation fromX " + stage.getWidth());
        //Due to threading issues, hbox.getWidth will sometimes return 0 below because this
        //method is being called before the hbox is rendered. hbox.applyCss() did not
        //fix this issue, so I have implemented the animationDurationMinder in the
        //main application. This is probably not ideal, but does work.
        transition.setToX(-hbox.getWidth());
        LOGGER.log(Level.DEBUG, "Animation toX " + -hbox.getWidth());
        transition.setDuration(Duration.seconds(hbox.getWidth() / stage.getWidth() * scrollSpeed));
        LOGGER.log(Level.DEBUG, "Animation duration set to " + Duration.seconds(hbox.getWidth() / stage.getWidth() * scrollSpeed));
        animationDurationMinder = new Timeline(
                new KeyFrame(Duration.millis(10),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                checkAnimationConfiguration();
                            }
                        }));
        animationDurationMinder.setCycleCount(Timeline.INDEFINITE);
        animationDurationMinder.play();
    }

    public void startAnimation() {
        transition.play();
    }

    public void restartAnimation() {
        transition.playFromStart();
    }

    public void stopAnimation() {
        transition.pause();
    }

    public TranslateTransition getTransition() {
        return transition;
    }

    public void setTransition(TranslateTransition transition) {
        this.transition = transition;
    }

    public double getHboxWidth() {
        return hbox.getWidth();
    }

    public void checkAnimationConfiguration() {

        if (transition.getToX() != (hbox.getWidth() * -1)) {
            LOGGER.log(Level.DEBUG, "Animation configuration is  out of sync: syncing now");
            buildAnimation();
            restartAnimation();
        }
    }
}
