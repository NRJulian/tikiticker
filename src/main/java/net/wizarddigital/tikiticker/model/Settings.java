/*
TikiTicker
Copyright (C) 2023 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.wizarddigital.tikiticker.model;

import java.util.ArrayList;

public class Settings {

    private int scrollSpeed = 50;
    private String fontName = "Arial";
    private int fontSize = 32;
    private ArrayList<String> rssUrls = new ArrayList<>();
    private double windowPositionX = 0;
    private double windowPositionY = 0;
    private double windowWidth = 600;
    private double windowHeight = 100;
    boolean useItemSeparatorString = true;
    String itemSeparatorString = " • ";
    int horizontalSpacing = 5;
    String backgroundColorInHex = "#b31a1a";
    String fontColorInHex = "#ffffff";
    int updateIntervalInMinutes = 60;
    boolean useTransparentBackground = false;

    public int getScrollSpeed() {
        return scrollSpeed;
    }

    public void setScrollSpeed(int scrollSpeed) {
        this.scrollSpeed = scrollSpeed;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public ArrayList<String> getRssUrls() {
        return rssUrls;
    }

    public void setRssUrls(ArrayList<String> rssUrls) {
        this.rssUrls = rssUrls;
    }

    public double getWindowPositionX() {
        return windowPositionX;
    }

    public void setWindowPositionX(double windowPositionX) {
        this.windowPositionX = windowPositionX;
    }

    public double getWindowPositionY() {
        return windowPositionY;
    }

    public void setWindowPositionY(double windowPositionY) {
        this.windowPositionY = windowPositionY;
    }

    public double getWindowWidth() {
        return windowWidth;
    }

    public void setWindowWidth(double windowWidth) {
        this.windowWidth = windowWidth;
    }

    public double getWindowHeight() {
        return windowHeight;
    }

    public void setWindowHeight(double windowHeight) {
        this.windowHeight = windowHeight;
    }

    public boolean getUseItemSeparatorString() {
        return useItemSeparatorString;
    }

    public void setUseItemSeparatorString(boolean useItemSeparatorString) {
        this.useItemSeparatorString = useItemSeparatorString;
    }

    public String getItemSeparatorString() {
        return itemSeparatorString;
    }

    public void setItemSeparatorString(String itemSeparatorString) {
        this.itemSeparatorString = itemSeparatorString;
    }

    public int getHorizontalSpacing() {
        return horizontalSpacing;
    }

    public void setHorizontalSpacing(int horizontalSpacing) {
        this.horizontalSpacing = horizontalSpacing;
    }

    public String getBackgroundColorInHex() {
        return backgroundColorInHex;
    }

    public void setBackgroundColorInHex(String backgroundColorInHex) {
        this.backgroundColorInHex = backgroundColorInHex;
    }

    public String getFontColorInHex() {
        return fontColorInHex;
    }

    public void setFontColorInHex(String fontColorInHex) {
        this.fontColorInHex = fontColorInHex;
    }

    public int getUpdateIntervalInMinutes() {
        return updateIntervalInMinutes;
    }

    public void setUpdateIntervalInMinutes(int updateIntervalInMinutes) {
        this.updateIntervalInMinutes = updateIntervalInMinutes;
    }

    public boolean isUseTransparentBackground() {
        return useTransparentBackground;
    }

    public void setUseTransparentBackground(boolean useTransparentBackground) {
        this.useTransparentBackground = useTransparentBackground;
    }

    public void addUrl(String urlString){
        this.rssUrls.add(urlString);
    }
}
