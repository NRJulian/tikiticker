/*
TikiTicker
Copyright (C) 2023 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.wizarddigital.tikiticker.util;

import com.apptasticsoftware.rssreader.Item;
import com.apptasticsoftware.rssreader.RssReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class RSSHandler {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    public ArrayList<Item> readFeed(String urlString) throws IOException {
        RssReader rssReader = new RssReader();
        ArrayList<Item> items = (ArrayList<Item>) rssReader.read(urlString).collect(Collectors.toList());
        //For some errors, the RssReader library doesn't throw exceptions but prints them. In that case,
        //the items.size() should be zero, so we'll throw our own exception.
        if (items.size() == 0) {
            throw new IOException();
        }
        return items;
    }
}
