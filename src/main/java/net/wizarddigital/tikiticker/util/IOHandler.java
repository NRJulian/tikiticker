/*
TikiTicker
Copyright (C) 2023 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.wizarddigital.tikiticker.util;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class IOHandler {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    public static String readFileToString(String filePath) throws IOException {
        String content;
        content = new String(Files.readAllBytes(Paths.get(filePath)));
        return content;
    }

    public static boolean fileExists(String filePathString) {
        File file = new File(filePathString);
        if (file.exists() && !file.isDirectory()) {
            return true;
        }
        return false;
    }

    public static boolean folderExists(String folderPathString) {
        File file = new File(folderPathString);
        if (file.exists() && file.isDirectory()) {
            return true;
        }
        return false;
    }

    public static void writeObjectToJsonFile(Object object, String filePathString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        mapper.writeValue(new File(filePathString), object);
    }

    public static Object readJsonFileToObject(String filePathString, Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        return mapper.readValue(new File(filePathString), object.getClass());
    }
}