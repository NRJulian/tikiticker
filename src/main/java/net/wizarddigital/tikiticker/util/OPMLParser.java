/*
TikiTicker
Copyright (C) 2023 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.wizarddigital.tikiticker.util;

import be.ceau.opml.OpmlParseException;
import be.ceau.opml.OpmlParser;
import be.ceau.opml.entity.Opml;
import be.ceau.opml.entity.Outline;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class OPMLParser {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    //This method takes the text read from an OPML file and extracts
    //the RSS URLS into a list.
    public ArrayList<String> parseOpml(String string) throws OpmlParseException {
        ArrayList<String> rssFeedsList = new ArrayList<>();
        Opml opml = null;
        opml = new OpmlParser().parse(string);
        for (Outline outline : opml.getBody().getOutlines()) {
            if (outline.getAttributes().containsKey("xmlUrl")) {
                rssFeedsList.add(outline.getAttributes().get("xmlUrl"));
            } else {
                for (Outline subElement : outline.getSubElements()) {
                    rssFeedsList.add(subElement.getAttribute("xmlUrl"));
                }
            }
        }
        return rssFeedsList;
    }
}
