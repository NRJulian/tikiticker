/*
TikiTicker
Copyright (C) 2023 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.wizarddigital.tikiticker.controller;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;

//This controller is used for the loading window shown when fetching RSS feeds.
//It is not shown during automatic timed updates.
//The fetching process is a background task, bound to the GUI as below
public class LoadingController {

    @FXML
    private ProgressBar progressBar;
    @FXML
    private TextArea loadingStatusTextArea;

    private Task task;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public void bindProgrssBar(ReadOnlyDoubleProperty property){
        progressBar.progressProperty().bind(property);
    }

    public void bindTextArea(ReadOnlyStringProperty property){
        loadingStatusTextArea.textProperty().bind(property);
    }

    public void onCancelButtonClicked(){
        task.cancel();

    }
}
