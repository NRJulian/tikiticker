/*
TikiTicker
Copyright (C) 2023 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.wizarddigital.tikiticker.controller;

import com.apptasticsoftware.rssreader.Item;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import net.wizarddigital.tikiticker.gui.DialogHandler;
import net.wizarddigital.tikiticker.gui.NewsTicker;
import net.wizarddigital.tikiticker.Application;
import net.wizarddigital.tikiticker.model.Settings;
import net.wizarddigital.tikiticker.util.ColorUtil;
import net.wizarddigital.tikiticker.util.IOHandler;
import net.wizarddigital.tikiticker.util.OPMLParser;
import net.wizarddigital.tikiticker.util.RSSHandler;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.dialog.FontSelectorDialog;
import org.apache.commons.validator.routines.UrlValidator;

import java.io.File;
import java.net.URL;
import java.util.*;

//This class is used to manage the Settings window, and maps
//changes to the window controls to the app's settings
public class SettingsController implements Initializable {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());
    Settings settings;
    private boolean rssUrlChangesApplied = true;
    ObservableList<String> observableRssUrls;
    private NewsTicker newsTicker;
    private Application mainApplication;

    @FXML
    private Button selectFontButton;
    @FXML
    private ColorPicker backgroundColorPicker;
    @FXML
    private ColorPicker fontColorPicker;
    @FXML
    private Slider scrollSpeedSlider;
    @FXML
    private ChoiceBox updateIntervalChoiceBox;
    @FXML
    private CheckBox invisibleBackgroundCheckbox;
    @FXML
    private TableView rssFeedTable;
    @FXML
    private ChoiceBox separationStringChoiceBox;
    @FXML
    private CheckBox useSeparationStringCheckBox;
    @FXML
    private ListView rssFeedListView;
    @FXML
    private Button importButton;
    @FXML
    private TextField addFeedTextField;
    @FXML
    private Button reloadFeedsNowButton;
    @FXML
    private Button moveFeedUpButton;
    @FXML
    private Button moveFeedDownButton;

    public boolean isRssUrlChangesApplied() {
        return rssUrlChangesApplied;
    }

    public void setRssUrlChangesApplied(boolean rssUrlChangesApplied) {
        this.rssUrlChangesApplied = rssUrlChangesApplied;
    }

    public Application getMainApplication() {
        return mainApplication;
    }

    public void setMainApplication(Application mainApplication) {
        this.mainApplication = mainApplication;
    }

    public NewsTicker getNewsTicker() {
        return newsTicker;
    }

    public void setNewsTicker(NewsTicker newsTicker) {
        this.newsTicker = newsTicker;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
        mapSettingsToGUI();
    }

    public void mapSettingsToGUI() {
        observableRssUrls = FXCollections.observableArrayList(settings.getRssUrls());
        this.scrollSpeedSlider.setValue(settings.getScrollSpeed());
        this.backgroundColorPicker.setValue(Color.valueOf(settings.getBackgroundColorInHex()));
        this.fontColorPicker.setValue(Color.valueOf(settings.getFontColorInHex()));
        updateIntervalChoiceBox.setValue(settings.getUpdateIntervalInMinutes());
        invisibleBackgroundCheckbox.setSelected(settings.isUseTransparentBackground());
        if (settings.isUseTransparentBackground()) {
            backgroundColorPicker.setDisable(true);
        } else {
            backgroundColorPicker.setDisable(false);
        }
        separationStringChoiceBox.setValue(settings.getItemSeparatorString());
        useSeparationStringCheckBox.setSelected(settings.getUseItemSeparatorString());
        if (settings.getUseItemSeparatorString()) {
            separationStringChoiceBox.setDisable(false);
        } else {
            separationStringChoiceBox.setDisable(true);
        }
        rssFeedListView.setItems(observableRssUrls);
    }

    @FXML
    protected void onScrollSpeedSliderChanged() {
        LOGGER.log(Level.DEBUG, "Speed set to " + (int) scrollSpeedSlider.getValue());
        int scrollSpeedSliderValue = (int) scrollSpeedSlider.getValue();
        settings.setScrollSpeed(scrollSpeedSliderValue);
        newsTicker.loadSettings(settings, true);
    }

    @FXML
    protected void onSelectFontButtonClicked() {
        FontSelectorDialog fontSelectorDialog = new FontSelectorDialog(null);
        fontSelectorDialog.setTitle("Select Font");
        fontSelectorDialog.show();
        fontSelectorDialog.setOnCloseRequest(e -> {
            if (fontSelectorDialog.getResult() != null) {
                Font font = fontSelectorDialog.getResult();
                settings.setFontName(font.getName());
                settings.setFontSize((int) font.getSize());
                newsTicker.loadSettings(settings, false);
            }
        });
    }

    @FXML
    protected void onInvisibleBackgroundValueChanged() {
        settings.setUseTransparentBackground(invisibleBackgroundCheckbox.isSelected());
        if (settings.isUseTransparentBackground()) {
            backgroundColorPicker.setDisable(true);
        } else {
            backgroundColorPicker.setDisable(false);
        }
    }

    @FXML
    protected void onUseSeparatorCheckboxValueChanged() {
        settings.setUseItemSeparatorString(useSeparationStringCheckBox.isSelected());
        if (settings.getUseItemSeparatorString()) {
            separationStringChoiceBox.setDisable(false);
            newsTicker.updateSeparatorLabels(settings.getItemSeparatorString());
        } else {
            separationStringChoiceBox.setDisable(true);
            newsTicker.updateSeparatorLabels("");
        }
    }

    @FXML
    protected void onBackgroundColorPickerEvent() {
        String backgroundColorInHex = ColorUtil.toRGBCode(backgroundColorPicker.getValue());
        settings.setBackgroundColorInHex(backgroundColorInHex);
        newsTicker.loadSettings(settings, false);
    }

    @FXML
    protected void onSeparationStringChoiceBoxValueChanged() {
        //Because the ChoiceBox is manipulated via code in Initialize, before the settings variable
        //is assigned, we need to check for null before trying to set the value within settings
        if (settings != null && newsTicker != null) {
            settings.setItemSeparatorString(separationStringChoiceBox.getValue().toString());
            newsTicker.loadSettings(settings, false);
        }
    }

    @FXML
    protected void onFontColorPickerEvent() {
        String fontColorInHex = ColorUtil.toRGBCode(fontColorPicker.getValue());
        settings.setFontColorInHex(fontColorInHex);
        newsTicker.loadSettings(settings, false);
    }

    @FXML
    protected void onUpdateIntervalChoiceBoxValueChanged() {
        //Because the ChoiceBox is manipulated via code in Initialize, before the settings variable
        //is assigned, we need to check for null before trying to set the value within settings
        if (settings != null) {
            settings.setUpdateIntervalInMinutes(Integer.parseInt(updateIntervalChoiceBox.getValue().toString()));
            if (mainApplication != null) {
                mainApplication.configureRssUpdateTask(settings.getUpdateIntervalInMinutes());
            }
            LOGGER.log(Level.DEBUG, "Update interval set to " + settings.getUpdateIntervalInMinutes());
        }
    }

    @FXML
    protected void onAddFeedButtonPressed() {
        String newRssFeedUrl = addFeedTextField.getText();
        UrlValidator validator = new UrlValidator();
        if (!validator.isValid(newRssFeedUrl)) {
            DialogHandler.showErrorDialog("Please enter a valid URL");
            return;
        }
        try {
            RSSHandler rssHandler = new RSSHandler();
            ArrayList<Item> items = rssHandler.readFeed(newRssFeedUrl);
        } catch (Exception e) {
            DialogHandler.showErrorDialog("Failed to fetch feed " + newRssFeedUrl + "\nPlease confirm it is a valid RSS/Atom feed.");
            return;
        }
        settings.getRssUrls().add(newRssFeedUrl);
        observableRssUrls.add(newRssFeedUrl);
        addFeedTextField.setText("");
        rssUrlChangesApplied = false;
    }

    @FXML
    protected void onImportButtonClicked() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("OPML Files", "*.opml")
        );
        File selectedFile = fileChooser.showOpenDialog(this.getNewsTicker().getStage());
        if (selectedFile != null) {
            OPMLParser opmlParser = new OPMLParser();
            ArrayList<String> rssUrls = null;
            try {
                rssUrls = opmlParser.parseOpml(IOHandler.readFileToString(selectedFile.getPath()));
            } catch (Exception e) {
                LOGGER.log(Level.ERROR, "Failed to parse OPML file: " + e.getMessage());
                DialogHandler.showErrorDialog("Failed to parse OPML file.");
                return;
            }
            observableRssUrls.removeAll(settings.getRssUrls());
            settings.setRssUrls(rssUrls);
            observableRssUrls.addAll(rssUrls);
            rssUrlChangesApplied = false;
        }
    }

    public void reloadFeedsNowButtonPressed() {
        mainApplication.readFeedsIntoNewsTicker(true, true);
        rssUrlChangesApplied = true;
    }

    public void onMoveFeedUpButtonClicked() {
        //First, confirm that the first selected item is not the first item on the list,
        //otherwise you can't move them up
        ObservableList selectedIndices = rssFeedListView.getSelectionModel().getSelectedIndices();
        if ((int) selectedIndices.get(0) == 0) {
            return;
        }
        //Move every item down one
        for (Object index : selectedIndices) {
            int value = (int) index;
            Collections.swap(settings.getRssUrls(), value, value - 1);
            Collections.swap(observableRssUrls, value, value - 1);
            rssFeedListView.getSelectionModel().clearSelection(value);
            rssFeedListView.getSelectionModel().select(value - 1);
        }
    }

    public void onMoveFeedDownButtonClicked() {
        //First, confirm that the last selected item is not the last item on the list,
        //otherwise you can't move them down
        ObservableList selectedIndices = rssFeedListView.getSelectionModel().getSelectedIndices();
        int lastSelectedIndex = (int) selectedIndices.get(selectedIndices.size() - 1);
        int lastItemIndex = rssFeedListView.getItems().size() - 1;
        if (lastSelectedIndex == lastItemIndex) {
            return;
        }
        //Have to convert the ObservableList into an ArrayList to use Collections.reverse()
        ArrayList<Integer> integers = new ArrayList<>();
        for (Object object : selectedIndices) {
            integers.add((int) object);
        }
        //Move every item down one
        Collections.reverse(integers);
        for (Object index : integers) {
            int value = (int) index;
            Collections.swap(settings.getRssUrls(), value, value + 1);
            Collections.swap(observableRssUrls, value, value + 1);
            rssFeedListView.getSelectionModel().clearSelection(value);
            rssFeedListView.getSelectionModel().select(value + 1);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        updateIntervalChoiceBox.getItems().add(5);
        updateIntervalChoiceBox.getItems().add(10);
        updateIntervalChoiceBox.getItems().add(15);
        updateIntervalChoiceBox.getItems().add(30);
        updateIntervalChoiceBox.getItems().add(60);
        updateIntervalChoiceBox.getItems().add(120);
        updateIntervalChoiceBox.getItems().add(240);
        updateIntervalChoiceBox.setValue(60);
        separationStringChoiceBox.getItems().add(" • ");
        separationStringChoiceBox.getItems().add(" * ");
        separationStringChoiceBox.getItems().add(" ~ ");
        separationStringChoiceBox.getItems().add(" - ");
        separationStringChoiceBox.getItems().add(" | ");
        separationStringChoiceBox.getItems().add(" ☠ ");
        updateIntervalChoiceBox.setValue(" • ");
        rssFeedListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        rssFeedListView.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode().equals(KeyCode.DELETE)) {
                    Object[] selectedUrls = rssFeedListView.getSelectionModel().getSelectedItems().toArray();
                    settings.getRssUrls().removeAll(List.of(selectedUrls));
                    observableRssUrls.removeAll(List.of(selectedUrls));
                    rssFeedListView.getSelectionModel().clearSelection();
                    rssUrlChangesApplied = false;
                }
            }
        });
    }

    @FXML
    protected void onSourceCodeHyperLinkClicked() {
        mainApplication.getHostServices().showDocument("https://gitlab.com/NRJulian/tikiticker/");
    }
}