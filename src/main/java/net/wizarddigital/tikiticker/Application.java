/*
TikiTicker
Copyright (C) 2023 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.wizarddigital.tikiticker;

import com.apptasticsoftware.rssreader.Item;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.wizarddigital.tikiticker.controller.LoadingController;
import net.wizarddigital.tikiticker.controller.SettingsController;
import net.wizarddigital.tikiticker.gui.DialogHandler;
import net.wizarddigital.tikiticker.gui.NewsTicker;
import net.wizarddigital.tikiticker.model.Settings;
import net.wizarddigital.tikiticker.util.IOHandler;
import net.wizarddigital.tikiticker.util.RSSHandler;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.MenuItem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Application extends javafx.application.Application {

    Stage stage;
    NewsTicker newsTicker;
    Settings settings;
    Scene scene;
    BorderPane basePane;
    SettingsController settingsController;
    Stage settingsStage;
    Timeline updateTimer;
    private final Logger LOGGER = LogManager.getLogger(this.getClass());
    private static final String SETTINGS_FILE_LOCATION = ".tikiticker/tikiticker_settings.json";

    @Override
    public void start(Stage stage) {
        LOGGER.log(Level.DEBUG, "Application started");
        this.stage = stage;
        readOrInitConfigFile();
        basePane = new BorderPane();
        newsTicker = new NewsTicker(stage);
        newsTicker.loadSettings(settings, true);
        readFeedsIntoNewsTicker(true, true);
        basePane.setCenter(newsTicker);
        scene = new Scene(basePane);
        scene.getStylesheets().add("style.css");
        stage.setScene(scene);
        configureStageSizeChangeListeners();
        loadStageSettings(settings);
        stage.setTitle("TikiTicker");
        stage.setOnCloseRequest(event -> {
            persistConfigToFile();
            Platform.exit();
        });
        stage.show();
        buildContextMenu();
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getResource("fxml/settings-view.fxml"));
        Scene settingsScene = null;
        try {
            settingsScene = new Scene(fxmlLoader.load());
        } catch (IOException e) {
            LOGGER.log(Level.ERROR, "Failed to load FXML for settings-view.fxml: " + e.getMessage());
            DialogHandler.showErrorDialog("An internal error occurred: program will now exit.\nSee error log for details.");
            Platform.exit();
        }
        settingsController = fxmlLoader.getController();
        settingsController.setMainApplication(this);
        settingsController.setSettings(settings);
        settingsController.setNewsTicker(newsTicker);
        settingsStage = new Stage();
        settingsStage.setTitle("TikiTicker Settings");
        settingsStage.setScene(settingsScene);
        settingsStage.setOnCloseRequest(event -> {
            persistConfigToFile();
            if (!settingsController.isRssUrlChangesApplied()) {
                readFeedsIntoNewsTicker(true, true);
            }
        });
        configureRssUpdateTask(settings.getUpdateIntervalInMinutes());
    }

    private void persistConfigToFile() {
        try {
            IOHandler.writeObjectToJsonFile(settings, SETTINGS_FILE_LOCATION);
        } catch (Exception e) {
            LOGGER.log(Level.ERROR, "Failed to save config file: " + e.getMessage());
        }
    }

    public void configureRssUpdateTask(int updateIntervalInMinutes) {
        updateTimer = new Timeline(
                new KeyFrame(Duration.minutes(updateIntervalInMinutes),
                        event -> {
                            readFeedsIntoNewsTicker(true, false);
                        }));
        updateTimer.setCycleCount(Timeline.INDEFINITE);
        updateTimer.play();
    }

    public void configureStageSizeChangeListeners() {
        stage.widthProperty().addListener((obs, oldVal, newVal) -> settings.setWindowWidth(stage.getWidth()));
        stage.heightProperty().addListener((obs, oldVal, newVal) -> settings.setWindowHeight(stage.getHeight()));
        stage.xProperty().addListener((obs, oldVal, newVal) -> settings.setWindowPositionX(stage.getX()));
        stage.yProperty().addListener((obs, oldVal, newVal) -> settings.setWindowPositionY(stage.getY()));
    }

    public void readOrInitConfigFile() {
        settings = new Settings();
        //If the config file exists
        if (IOHandler.fileExists(SETTINGS_FILE_LOCATION)) {
            LOGGER.log(Level.DEBUG, "Found config file at " + SETTINGS_FILE_LOCATION + ". will attempt to load settings");
            //try to load it
            try {
                this.settings = (Settings) IOHandler.readJsonFileToObject(SETTINGS_FILE_LOCATION, settings);
                LOGGER.log(Level.DEBUG, "Loaded settings successfully");
                return;
            } catch (Exception e) {
                LOGGER.log(Level.ERROR, "Failed to load settings file! " + e.getMessage());
            }
        }
        //Otherwise, try to create a new one
        try {
            //Set the default width to the screen width, since we don't have a saved value
            settings.setWindowWidth((int) Screen.getPrimary().getBounds().getWidth());
            //Write the file
            if (!IOHandler.folderExists(".tikiticker")) {
                new File(".tikiticker").mkdirs();
            }
            IOHandler.writeObjectToJsonFile(settings, SETTINGS_FILE_LOCATION);
            LOGGER.log(Level.ERROR, "New Settings file created at " + SETTINGS_FILE_LOCATION);

        } catch (Exception e) {
            LOGGER.log(Level.ERROR, "Failed to create config directory or config file in run location! Config will not be saved. Please run in a location where you have write permission.");
        }
    }

    public void loadNewsTickerLinks(ArrayList<Hyperlink> links, boolean clearOldLinks) {
        if (clearOldLinks) {
            newsTicker.clearAllLinksAndLabels();
        }
        newsTicker.addHyperlinks(links);
        newsTicker.applySettings(true);
    }

    public void readFeedsIntoNewsTicker(boolean clearOldLinks, boolean showLoadingWindow) {
        LOGGER.log(Level.DEBUG, "Initiating read of RSS feeds");
        //Load the Loading Scene FXML
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getResource("fxml/loading-view.fxml"));
        Scene loadingScene = null;
        try {
            loadingScene = new Scene(fxmlLoader.load());
        } catch (IOException e) {
            LOGGER.log(Level.ERROR, "Failed to load FXML for loading-view.fxml: " + e.getMessage());
            DialogHandler.showErrorDialog("An internal error occurred: program will now exit.\nSee error log for details.");
            Platform.exit();
        }
        Stage loadingStage = new Stage();
        loadingStage.setScene(loadingScene);
        LoadingController controller = fxmlLoader.getController();
        if (showLoadingWindow) {
            loadingStage.show();
        }
        //This is the long-running background task that reads the RSS feeds
        Task<Void> task = new Task<>() {
            boolean anyFeedsFailed = false;
            ArrayList<String> failedUrls = new ArrayList<>();

            @Override
            protected Void call() {
                int totalRssUrls = settings.getRssUrls().size();
                int processedRssUrls = 0;
                String status = "";
                ArrayList<Hyperlink> links = new ArrayList<>();
                RSSHandler rssHandler = new RSSHandler();
                for (String url : settings.getRssUrls()) {
                    ArrayList<Item> items = new ArrayList<>();
                    //Status bound to Loading Scene
                    status = "Processing " + url + "\n" + status;
                    updateMessage(status);
                    try {
                        items = rssHandler.readFeed(url);
                    } catch (Exception e) {
                        //Apparently cancelling the task triggers this exception block,
                        //so we have to check that the task (this) is NOT cancelled
                        //in order to catch legitimate loading fails
                        if (!this.isCancelled()) {
                            failedUrls.add(url);
                            anyFeedsFailed = true;
                            continue;
                        }
                    }
                    for (Item item : items) {
                        if (item.getTitle().isPresent() && item.getLink().isPresent()) {
                            Hyperlink link = new Hyperlink(item.getTitle().get());
                            link.setOnAction(new EventHandler<>() {
                                @Override
                                public void handle(ActionEvent t) {
                                    getHostServices().showDocument(item.getLink().get());
                                }
                            });
                            links.add(link);
                        }
                    }
                    processedRssUrls += 1;
                    updateProgress(processedRssUrls, totalRssUrls);
                }
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        //Alert the user if any feeds failed to load (or were empty)
                        if (anyFeedsFailed) {
                            if (showLoadingWindow) {
                                String formattedUrls = "";
                                for (String url : failedUrls) {
                                    formattedUrls = formattedUrls + url + "\n";
                                }
                                DialogHandler.showErrorDialog("The following feeds were empty or failed to load: " + "\n\n" + formattedUrls);
                            }
                        }
                        loadNewsTickerLinks(links, clearOldLinks);
                    }
                });
                return null;
            }
        };
        task.setOnFailed(wse -> {
            LOGGER.log(Level.ERROR, "Fatal error occurred processing feeds: " + wse.getSource().getException().getMessage());
            DialogHandler.showErrorDialog("An internal error occurred: program will now exit.\nSee error log for details.");
            Platform.exit();
        });
        task.setOnSucceeded(wse -> loadingStage.close());
        task.setOnCancelled(wse -> loadingStage.close());
        controller.setTask(task);
        controller.bindProgrssBar(task.progressProperty());
        controller.bindTextArea(task.messageProperty());
        loadingStage.setOnCloseRequest(event -> {
            task.cancel();
        });
        new Thread(task).start();
    }

    //This is the context menu to be applied to the NewsTicker
    private void buildContextMenu() {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem optionsMenuItem = new MenuItem("Options");
        optionsMenuItem.setOnAction((ActionEvent e) -> {
            settingsController.mapSettingsToGUI();
            settingsStage.show();
        });
        MenuItem exitMenuItem = new MenuItem("Exit");
        exitMenuItem.setOnAction((ActionEvent e) -> Platform.exit());
        contextMenu.getItems().add(optionsMenuItem);
        contextMenu.getItems().add(exitMenuItem);
        newsTicker.addContextMenu(contextMenu);
    }

    public void loadStageSettings(Settings settings) {
        stage.setX(settings.getWindowPositionX());
        stage.setY(settings.getWindowPositionY());
        stage.setWidth(settings.getWindowWidth());
        stage.setHeight(settings.getWindowHeight());
        if (settings.isUseTransparentBackground()) {
            stage.initStyle(StageStyle.TRANSPARENT);
            scene.setFill(Color.TRANSPARENT);
            basePane.setStyle("fx-background-color: transparent");
            newsTicker.activateInvisibleBackground();
        }
    }

    public static void main(String[] args) {
        launch();
    }
}