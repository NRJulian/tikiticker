# TikiTicker
TikiTicker is a simple desktop application for scrolling RSS and Atom feeds across your screen like a TV news ticker.

![Screenshot](./images/screenshot.png)
*TikiTicker scrolling some cybersecurity feeds and displaying the 'Settings' dialog*

Features:
- Extensive customization of font, color and size of feeds
- Invisible background option
- Import your feeds from an OPML file
- Runs on Linux and Windows (and would on Mac, if someone with a Mac would build it for me :)
- 100% [FOSS software](https://itsfoss.com/what-is-foss/)

## Installation

TikiTicker is a portable Java desktop application and can be run by simply downloading and double-clicking the most recent JAR file available on the releases page.
Note that you must download the appropriate jar file for your operating system.
You will need to have a Java runtime installed on your machine. You can download AdoptOpenJDK [here](https://adoptopenjdk.net/),
although lately I have been fond of the [Azul Zulu builds](https://www.azul.com/downloads/) which include JavaFX by default.

## Usage:
1. Download the latest jar file from the releases page. **Note: you must download the appropriate jar for your OS: Linux or Windows.** 
2. Move the jar file to wherever you want to install the program on your system. You will need to build your own shortcuts and start menu items, if you want those.
3. Double-click the .jar file to run it. You can resize and move the ticker window however you like, and you can right-click on the application window to access the settings window.
**Note: activating the invisible background requires a restart of the application and will remove the window controls.
You can still access the settings in invisible background mode by right-clicking on the text on your screen.**

## Licensing:

TikiTicker is written entirely in Java and is free (libre) software licensed under the GNU GPL v.3.0 license. TikiTicker uses the following open-source libraries:
- [Apache Commons Validator](https://github.com/apache/commons-validator) ([Apache 2.0](https://github.com/apache/commons-validator/blob/master/LICENSE.txt))
- [ControlsFX](https://github.com/controlsfx/controlsfx) ([BSD 3-Clause "New" or "Revised" License](https://github.com/controlsfx/controlsfx/blob/master/license.txt))
- [Jackson Databind](https://github.com/FasterXML/jackson-databind) ([Apache 2.0](https://github.com/FasterXML/jackson-databind/blob/master/LICENSE))
- [Log4j2](https://github.com/apache/logging-log4j2) ([Apache 2.0](https://github.com/apache/logging-log4j2/blob/release-2.x/LICENSE.txt))
- [OpenJFX](https://github.com/openjdk/jfx) ([GPL v2.0](https://github.com/openjdk/jfx/blob/master/LICENSE))
- [OPML Parser](https://github.com/mdewilde/opml-parser) ([Apache 2.0](https://github.com/mdewilde/opml-parser/blob/master/LICENSE))
- [RSSReader](https://github.com/w3stling/rssreader) ([MIT License](https://github.com/w3stling/rssreader/blob/master/LICENSE))

TikiTicker is written using Intellij IDE and Maven. As such, the end user can replace or upgrade any of these libraries by opening the project in NetBeans, editing the dependency details in the `pom.xml` file, and rebuilding the project. Details can be found here:
- [Using Intellij and Maven](https://www.jetbrains.com/help/idea/maven-support.html)